<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cv;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;


class CvController extends Controller
{
    /**
     * Lists all cv entities.
     *
     * @Route("/cvs", name="cvshow")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cvs = $em->getRepository('AppBundle:Cv')->findAll();

        return $this->render('cv/index.html.twig', array(
            'cvs' => $cvs,
        ));
    }
}
