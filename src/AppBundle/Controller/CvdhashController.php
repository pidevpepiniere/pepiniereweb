<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cv;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Cv controller.
 *
 * @Route("cv")
 */
class CvdhashController extends Controller
{
    /**
     * Lists all cv entities.
     *
     * @Route("/", name="cv_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $cvs = $em->getRepository('AppBundle:Cv')->findBy(array('idUser'=>$user));

        return $this->render('cv/Cv_dash.html.twig', array(
            'cvs' => $cvs,
        ));
    }

    /**
     * Creates a new cv entity.
     *
     * @Route("/new", name="cv_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {if (!$this->get('security.authorization_checker')->isGranted('ROLE_JARDNIER')) {
        throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
    }
        $cv = new Cv();
        $form = $this->createForm('AppBundle\Form\CvType', $cv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file=$cv->getImage();
            $filename="uploads/jardinier/photo/".md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('image_directory'),$filename);
            $cv->setImage($filename);
            $em = $this->getDoctrine()->getManager();
            $em->persist($cv);
            $em->flush();

            return $this->redirectToRoute('cv_show', array('idCv' => $cv->getIdcv()));
        }

        return $this->render('cv/new.html.twig', array(
            'cv' => $cv,
            'form' => $form->createView(),
            'idUser'=>(string)$this->getUser()->getId()
        ));
    }

    /**
     * Finds and displays a cv entity.
     *
     * @Route("/{idCv}", name="cv_show")
     * @Method("GET")
     */
    public function showAction(Cv $cv)
    {
        $deleteForm = $this->createDeleteForm($cv);

        return $this->render('cv/show.html.twig', array(
            'cv' => $cv,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cv entity.
     *
     * @Route("/{idCv}/edit", name="cv_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Cv $cv)
    {
        $deleteForm = $this->createDeleteForm($cv);
        $editForm = $this->createForm('AppBundle\Form\CvType', $cv);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cv_edit', array('idCv' => $cv->getIdcv()));
        }

        return $this->render('cv/edit.html.twig', array(
            'cv' => $cv,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cv entity.
     *
     * @Route("/{idCv}", name="cv_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Cv $cv)
    {if (!$this->get('security.authorization_checker')->isGranted('ROLE_JARDNIER')) {
        throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
    }
        $form = $this->createDeleteForm($cv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cv);
            $em->flush();
        }

        return $this->redirectToRoute('cv_index');
    }

    /**
     * Creates a form to delete a cv entity.
     *
     * @param Cv $cv The cv entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cv $cv)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cv_delete', array('idCv' => $cv->getIdcv())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


}
