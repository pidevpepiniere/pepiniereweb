<?php
/**
 * Created by PhpStorm.
 * Date: 27/03/2019
 * Time: 13:45
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Reclamation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class ReclamationController extends Controller
{
    /**
     * @Route("/da/reclamation", name="reclamation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        //$user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $reclamations = $em->getRepository('AppBundle:Reclamation')->findAll();

        return $this->render('@App/Reclamation/dashboard/indexreclamation.html.twig', array(
            'reclamations' => $reclamations,
        ));
    }

    /**
     * @Route("/da/reclamation/new", name="reclamation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request ,\Swift_Mailer $mailer )
    {
        $em = $this->getDoctrine()->getManager();
        $reclamation  = new Reclamation();
        $user         = $this->get('security.token_storage')->getToken()->getUser();
        $reclamation->setIdUser($user);
        $form = $this->createForm('AppBundle\Form\ReclamationType', $reclamation);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reclamation);
            $em->flush();
            $message = (new \Swift_Message('Hello Email'))
                ->setFrom('samibouaben76@gmail.com')
                ->setTo('koussay.belaid@esprit.tn')
                ->setBody(
                   "hello",
                    'text/html'
                )
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'Emails/registration.txt.twig',
                        ['name' => $name]
                    ),
                    'text/plain'
                )
                */
            ;

            $mailer->send($message);
            return $this->redirectToRoute('reclamation_new');
        }
        return $this->render('@App/Reclamation/dashboard/newreclamation.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/da/reclamation/delete/{id}", name="reclamation_delete")
     * @Method({"GET", "POST"})
     */
    public function deletAction(Request $request, $id)
    {
        $em            = $this->getDoctrine()->getManager();
        var_dump($em);
        $reclamation   = $this->getDoctrine()->getRepository('AppBundle:Reclamation')->find($id);
        var_dump($reclamation);
        $em->remove($reclamation);


        $em->flush();

        return $this->redirectToRoute('reclamation_index');
    }

    /**
     * @Route("/reclamation/{id}", name="reclamation_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $reclamations = $em->getRepository('AppBundle:Reclamation')->find($id);
        return $this->render('@App/Reclamation/dashboard/showreclamation.html.twig', array(
            'reclamation' => $reclamations,
        ));
    }


}