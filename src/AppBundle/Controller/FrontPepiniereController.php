<?php
/**
 * Created by PhpStorm.
 * User: arafe
 * Date: 04/04/2019
 * Time: 22:43
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Finder\Exception\AccessDeniedException;


class FrontPepiniereController extends Controller
{
    /**
     * @Route("/pepiniere", name="front_pepiniere_index")
     * @Method("GET")
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $pepinieres = $em->getRepository('AppBundle:Pepinieres')->findAll();
        //$categories = $em->getRepository('AppBundle:CategorieProd')->findAll();
        return $this->render('@App/Pepiniere/front/indexpepiniere.html.twig', array(
            'pepinieres' => $pepinieres,
            //'categories' => $categories,
        ));
    }

    /**
     * @Route("/pepiniere/{id}", name="front_pepiniere_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $pepinieres = $em->getRepository('AppBundle:Pepinieres')->find($id);
        return $this->render('@App/Pepiniere/front/detailpepiniere.html.twig', array(
            'pepinieres' => $pepinieres,
        ));
    }

    /**
     *
     * @Route("/recherchePepiniere", name="front_pepiniere_recherche")
     * @Method({"GET", "POST"})
     */
    public function rechercheAction(Request $request)
    {
        $em      = $this->getDoctrine()->getManager();
        $keyWord = $request->get('keyWord');

        $pepinieres = $em->getRepository('AppBundle:Pepinieres')->findPep($keyWord);

        $template = $this->render(
            '@App/Pepiniere/front/recherchepepinier.html.twig',
            [
                'pepinieres' => $pepinieres
                ,
            ]
        )->getContent();

        $json     = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }



}