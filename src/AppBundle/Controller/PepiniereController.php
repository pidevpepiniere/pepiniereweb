<?php
/**
 * Created by PhpStorm.
 * User: arafe
 * Date: 04/04/2019
 * Time: 20:29
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Pepinieres;
use AppBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class PepiniereController extends Controller
{
    /**
     * @Route("/da/pepiniere", name="da_pepiniere_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $pepiniere = $em->getRepository('AppBundle:Pepinieres')->findAll();
        //$categories = $em->getRepository('AppBundle:CategorieProd')->findAll();
        return $this->render('@App/Pepiniere/dashboard/indexpepiniere.html.twig', array(
            'pepinieres' => $pepiniere,
            //'categories' => $categories,
        ));
    }

    /**
     * @Route("/da/pepiniere/new", name="da_pepiniere_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $pepiniere = new Pepinieres();
        $form = $this->createForm('AppBundle\Form\PepinieresType', $pepiniere);
        $form->handleRequest($request);
        //$pepiniere->setDisponibilite("dispo");

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pepiniere);
            $em->flush();
            return $this->redirectToRoute('da_pepiniere_index');
        }
        return $this->render('@App/Pepiniere/dashboard/newpepiniere.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/da/pepiniere/{id}/edit", name="da_pepiniere_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Pepinieres $pep)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $form = $this->createForm('AppBundle\Form\PepinieresType', $pep);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('da_pepiniere_index');
            //return $this->redirectToRoute('da_pepiniere_edit', array('id' => $pep->getIdPep()));
        }

        return $this->render('@App/Pepiniere/dashboard/editpepiniere.html.twig', array(
            'pepiniere' => $pep,
            'edit_form' => $form->createView(),
        ));
    }

    /**
     * @Route("/da/pepiniere/{id}", name="da_pepiniere_consulter")
     * @Method("GET")
     */
    public function showAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $pepinieres = $em->getRepository('AppBundle:Pepinieres')->findAll();
        return $this->render('@App/Pepiniere/dashboard/indexpepiniere.html.twig', array(
            'pepinieres' => $pepinieres,
        ));
    }

    /**
     * @Route("/da/pepiniere/delete/{id}", name="da_pepiniere_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $pepiniere = $this->getDoctrine()->getRepository('AppBundle:Pepinieres')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($pepiniere);
        $em->flush();
        return $this->redirectToRoute('da_pepiniere_index');
    }

    /**
     * @Route("/da/pepiniere/reaprovisionner/{id}", name="da_pepiniere_reaprovisionner")
     * @Method("GET")
     */
    public function reaprovisionnerAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $pepiniere = $em->getRepository('AppBundle:Pepiniere')->findAll();
        return $this->render('@App/Pepiniere/dashboard/indexpepiniere.html.twig', array(
            'pepiniere' => $pepiniere,
        ));
    }

}