<?php
/**
 * Created by PhpStorm.
 * User: arafe
 * Date: 11/04/2019
 * Time: 08:57
 */

namespace AppBundle\Controller;

use AppBundle\Entity\LigneDePanier;
use AppBundle\Entity\Panier;
use AppBundle\Entity\Produit;
use AppBundle\Entity\User;
use Knp\Bundle\PaginatorBundle\Definition\PaginatorAware;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class PanierController extends Controller
{
    /**
     * @Route("/panier", name="front_panier_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $panier = $em->getRepository('AppBundle:Panier')->findOneBy(array('user'=> $user), array('id'=> 'DESC'));
        return $this->render('@App/Produit/front/panier.html.twig', array(
            'panier' => $panier,
        ));
    }

    /**
     * @Route("/panier/retirer/{id}", name="retirer_ligne")
     * @Method("GET")
     */
    public function retirerLigneDePanierAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $ligneProduit = $this->getDoctrine()->getManager()->getRepository('AppBundle:LigneDePanier')->find($id);
        $produit = $ligneProduit->getProduit();
        $produit->setStock($produit->getStock() + $ligneProduit->getQuantite());
        $panier = $ligneProduit->getPanier();

        $em->remove($ligneProduit);
        $em->flush();
        $newNbrProd = $this->calculerNbrProduitByUser($user, $panier);
        $newTotal = $this->calculerPrixTotal($panier);
        $panier->setNbrProduit($newNbrProd);
        $panier->setTotal($newTotal);
        $em->flush();

        $em->remove($produit);
        $em->remove($panier);

        return $this->render('@App/Produit/front/panier.html.twig', array(
            'panier' => $panier,
        ));
    }
    /**
     * @Route("/panier/retirerAjax", name="retirer_ligne_ajax")
     * @Method("GET")
     */
    public function retirerLigneDePanierAjaxAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $ligneProduit = $this->getDoctrine()->getManager()->getRepository('AppBundle:LigneDePanier')->find($id);
        $produit = $ligneProduit->getProduit();
        $produit->setStock($produit->getStock() + $ligneProduit->getQuantite());
        $panier = $ligneProduit->getPanier();

        $em->remove($ligneProduit);
        $em->flush();
        $newNbrProd = $this->calculerNbrProduitByUser($user, $panier);
        $newTotal = $this->calculerPrixTotal($panier);
        $panier->setNbrProduit($newNbrProd);
        $panier->setTotal($newTotal);
        $em->flush();

        $em->remove($produit);
        $em->remove($panier);

        $template = $this->render(
            '@App/Produit/front/panierAjax.html.twig',
            [
                'panier' => $panier,
            ]
        )->getContent();

        $json     = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function calculerNbrProduitByUser(User $u, Panier $p){
        $allLignes =  $this->getDoctrine()->getManager()->getRepository('AppBundle:LigneDePanier')->findAll();
        $nbr = 0;
        foreach ($allLignes as $l) {
            if ($l->getPanier() == $p) {
                $nbr =$nbr + $l->getQuantite();
            }
        }
        return $nbr;
    }

    public function calculerPrixTotal(Panier $p){
        $allLignes =  $this->getDoctrine()->getManager()->getRepository('AppBundle:LigneDePanier')->findAll();
        $prix = 0;
        foreach ($allLignes as $l) {
            if ($l->getPanier() == $p) {
                $prix = $prix + ($l->getProduit()->getPrix() * $l->getQuantite());
            }
        }
        return $prix;
    }

    /**
     * @Route("/panier/confirme", name="panier_confirmer")
     * @Method("GET")
     */
    public function confirmerCommandeAction()
    {
        return $this->render('@App/Produit/front/panierCofirmation.html.twig');
    }

    /**
     * @Route("/panier/paiement/{id}", name="paiement")
     * @Method("GET")
     */
    public function paiementAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $panier = $em->getRepository('AppBundle:Panier')->find($id);
        $panier->setEtat("confirmé");
        $panier->setConfirmedAt(new \DateTime('now'));
        $em->persist($panier);

        $newPanier = new Panier();
        $newPanier->setUser($user);
        $newPanier->setEtat('en cours');
        $newPanier->setTotal(0);
        $newPanier->setNbrProduit(0);
        $em->persist($newPanier);
        $em->flush();

        \Stripe\Stripe::setApiKey('sk_test_Od7cBAHg24ewzEtpBXPGOvA8008l8TuiRR');
        \Stripe\Charge::create(array(
            "amount" => $panier->getTotal() * 100,
            "currency" => "eur",
            "source" => "tok_visa",
            "description" => "Paiement de test "
        ));

        return $this->render('@App/Produit/front/paiement.html.twig');
    }

}