<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Offredemande;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Offredemande controller.
 *
 * @Route("offredemande")
 */
class OffredemandeController extends Controller
{
    /**
     * Lists all offredemande entities.
     *
     * @Route("/", name="offredemande_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
if ($user.$this->isGranted('ROLE_JARDNIER')) {
    $offredemandes = $em->getRepository('AppBundle:Offredemande')->findBy(array('idUser' => $user));
}
elseif (    $user.$this->isGranted('ROLE_JARDNIER'))
{$offredemandes = $em->getRepository('AppBundle:Offredemande')->findAll();}
        return $this->render('offredemande/offre_dash.html.twig', array(
            'offredemandes' => $offredemandes,
        ));
    }

    /**
     * Creates a new offredemande entity.
     *
     * @Route("/new/{idAn}", name="offredemande_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request,$idAn)
    {
        $offredemande = new Offredemande();
        $form = $this->createForm('AppBundle\Form\OffredemandeType', $offredemande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($offredemande);
            $em->flush();

            return $this->redirectToRoute('offredemande_index');
        }

        return $this->render('offredemande/new.html.twig', array(
            'offredemande' => $offredemande,
            'form' => $form->createView(),
            'idAn'=>$idAn,
            'idUser'=>(string)$this->getUser()->getId()

        ));
    }

    /**
     * Finds and displays a offredemande entity.
     *
     * @Route("/{idAn}", name="offredemande_show")
     * @Method("GET")
     */
    public function showAction($idAn)
    {
        $em = $this->getDoctrine()->getManager();
            $offredemandes = $em->getRepository('AppBundle:Offredemande')->findBy(array('idAn' => $idAn));
        return $this->render('offredemande/index.html.twig', array(
            'offredemandes' => $offredemandes,
        ));
        }

    /**
     * Displays a form to edit an existing offredemande entity.
     *
     * @Route("/{idOffre}/edit", name="offredemande_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Offredemande $offredemande)
    {
        $deleteForm = $this->createDeleteForm($offredemande);
        $editForm = $this->createForm('AppBundle\Form\OffredemandeType', $offredemande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('offredemande_edit', array('idOffre' => $offredemande->getIdoffre()));
        }

        return $this->render('offredemande/edit.html.twig', array(
            'offredemande' => $offredemande,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a offredemande entity.
     *
     * @Route("/{idOffre}", name="offredemande_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Offredemande $offredemande)
    {
        $form = $this->createDeleteForm($offredemande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($offredemande);
            $em->flush();
        }

        return $this->redirectToRoute('offredemande_index');
    }

    /**
     * Creates a form to delete a offredemande entity.
     *
     * @param Offredemande $offredemande The offredemande entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Offredemande $offredemande)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('offredemande_delete', array('idOffre' => $offredemande->getIdoffre())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
