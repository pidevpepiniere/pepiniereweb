<?php
/**
 * Created by PhpStorm.
 * User: arafe
 * Date: 04/04/2019
 * Time: 20:29
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class DAProduitController extends Controller
{
    /**
     * @Route("/da/produit", name="da_produit_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('AppBundle:Produit')->findAll();
        $categories = $em->getRepository('AppBundle:CategorieProd')->findAll();
        return $this->render('@App/Produit/dashboard/index.html.twig', array(
            'produits' => $produits,
            'categories' => $categories,
        ));
    }

    /**
     * @Route("/da/produit/new", name="da_produit_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $produit = new Produit();
        $form = $this->createForm('AppBundle\Form\ProduitType', $produit);
        $form->handleRequest($request);
        $produit->setDisponibilite("dispo");

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();
            return $this->redirectToRoute('da_produit_index');
        }
        return $this->render('@App/Produit/dashboard/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/da/produit/{id}/edit", name="da_produit_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Produit $produit)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $form = $this->createForm('AppBundle\Form\ProduitType', $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('da_produit_edit', array('id' => $produit->getIdProd()));
        }

        return $this->render('@App/Produit/dashboard/edit.html.twig', array(
            'produit' => $produit,
            'edit_form' => $form->createView(),
        ));
    }

    /**
     * @Route("/da/produit/{id}", name="da_produit_consulter")
     * @Method("GET")
     */
    public function showAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('AppBundle:Produit')->findAll();
        return $this->render('@App/Produit/dashboard/index.html.twig', array(
            'produits' => $produits,
        ));
    }

    /**
     * @Route("/da/produit/delete/{id}", name="da_produit_delete")
     * @Method({"GET", "DELETE"})
     */
    public function deleteAction(Request $request, $id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $produit = $this->getDoctrine()->getRepository('AppBundle:Produit')->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($produit);
        $em->flush();
        return $this->redirectToRoute('da_produit_index');
    }

    /**
     * @Route("/da/produit/reaprovisionner/{id}", name="da_produit_reaprovisionner")
     * @Method("GET")
     */
    public function reaprovisionnerAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('AppBundle:Produit')->findAll();
        return $this->render('@App/Produit/dashboard/index.html.twig', array(
            'produits' => $produits,
        ));
    }

    /**
     * @Route("/da/commandes", name="da_commandes_index")
     * @Method("GET")
     */
    public function commandesAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $paniers = $em->getRepository('AppBundle:Panier')->findBy(array('etat'=> "confirmé"));
        return $this->render('@App/Produit/dashboard/commandes.html.twig', array(
            'paniers' => $paniers,
        ));
    }

    /**
     * @Route("/da/commandes/delete/{id}", name="da_panier_delete")
     * @Method("GET")
     */
    public function commandesDeleteAction($id)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException("Vous n'êtes pas autorisés à accéder à cette page!", Response::HTTP_FORBIDDEN);
        }
        $em = $this->getDoctrine()->getManager();
        $panier = $em->getRepository('AppBundle:Panier')->find($id);
        $em->remove($panier);
        $em->flush();
        return $this->redirectToRoute('da_commandes_index');

    }


}