<?php
/**
 * Created by PhpStorm.
 * User: arafe
 * Date: 04/04/2019
 * Time: 22:43
 */

namespace AppBundle\Controller;

use AppBundle\Entity\LigneDePanier;
use AppBundle\Entity\Panier;
use AppBundle\Entity\Produit;
use AppBundle\Entity\User;
use Knp\Bundle\PaginatorBundle\Definition\PaginatorAware;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Finder\Exception\AccessDeniedException;


class FProduitController extends Controller
{
    /**
     * @Route("/produit", name="front_produit_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $allProduits = $em->getRepository('AppBundle:Produit')->findAll();
        $paginator    = $this->get('knp_paginator');
        $produits = $paginator->paginate(
            $allProduits,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 3)
        );
        $categories = $em->getRepository('AppBundle:CategorieProd')->findAll();
        return $this->render('@App/Produit/front/index.html.twig', array(
            'produits' => $produits,
            'categories' => $categories,
        ));
    }

    /**
     * @Route("/produit/{id}", name="front_produit_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository('AppBundle:Produit')->find($id);
        return $this->render('@App/Produit/front/detail.html.twig', array(
            'produit' => $produit,
        ));
    }


    /**
     *
     * @Route("/panier/nbrProduit", name="front_panier_nbProduit")
     * @Method({"GET", "POST"})
     */
    public function getPanierNombreProduitAction(Request $request)
    {
        $em               = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $panier = $this->getDoctrine()->getManager()->getRepository('AppBundle:Panier')
                       ->findOneBy(array('user'=> $user), array('id'=> 'DESC'));
        $nbProd = $panier->getNbrProduit();
        if (($request->isXmlHttpRequest())) {
            $arrData = ['nbrProduit' => $nbProd];
        }
        return new JsonResponse($arrData);

    }


    /**
     *
     * @Route("/addToPanier/produit/{id}", name="front_add_to_panier_old")
     * @Method({"GET", "POST"})
     */
    public function addProduitToCartOldAction(Request $request, $id)
    {
        $ligne = new LigneDePanier();
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $qte = $request->get('qte');
        $produit = $em->getRepository('AppBundle:Produit')->find($id);
        $panier = $this->getDoctrine()->getManager()->getRepository('AppBundle:Panier')->findOneBy(array('user'=> $user), array('id'=> 'DESC'));
//        $panier = $this->getDoctrine()->getManager()->getRepository('AppBundle:Panier')->findLastPanierInsertedByUser($user);

        //l'utilisateur posséde un panier
        if(($panier )&& ($panier->getEtat() != "confirmé") ) {
            // Le produit existe fel panier , donc nzid l qte , w nahseb l panier.total + panier.nbrProduit
            if ($this->produitExist($panier, $produit) == true){
                $ligneProduit = $this->getDoctrine()->getManager()->getRepository('AppBundle:LigneDePanier')->findOneBy(array('produit'=> $produit, 'panier'=>$panier));
                $ligneProduit->setQuantite($ligneProduit->getQuantite() + $qte);

                $newNbrProd = $this->calculerNbrProduitByUser($user, $panier);
                $panier->setNbrProduit($newNbrProd);
                //n3awend na7seb panier.total
                $newTotalPrix = $this->calculerPrixTotal($panier);
                $panier->setTotal($newTotalPrix);

                $produit->setStock($produit->getStock()-$qte);
                $em->persist($produit);
                $em->persist($ligneProduit);
                $em->persist($panier);
                $em->flush();
            } else {
                $ligne = new LigneDePanier();
                $ligne->setPanier($panier);
                $ligne->setProduit($produit);
                $ligne->setQuantite($qte);
                $em->persist($ligne);
                $em->flush();

                //na7seb l panier.nbrProd , w l panier.total men jdiid
                $newNbrProd = $this->calculerNbrProduitByUser($user, $panier);
                $panier->setNbrProduit($newNbrProd);
                $newTotalPrix = $this->calculerPrixTotal($panier);
                $panier->setTotal($newTotalPrix);
                $produit->setStock($produit->getStock()-$qte);
                $em->persist($produit);
                $em->persist($panier);
                $em->flush();
            }
        }
        else { //ma3andouch panier
            $panier = new Panier();
            $ligne = new LigneDePanier();
            $panier->setUser($user);
            $panier->setNbrProduit($qte);
            $panier->setTotal($produit->getPrix() * $qte);

            $ligne->setPanier($panier);
            $ligne->setProduit($produit);
            $ligne->setQuantite($qte);
            $ligne->setPanier($panier);

            $produit->setStock($produit->getStock()-$qte);
            $em->persist($produit);
            $em->persist($ligne);
            $em->persist($panier);
            $em->flush();
        }

        $template = $this->render(
            '@App/Produit/front/productAjax.html.twig',
            [
                'produit' => $produit,
            ]
        )->getContent();

        $json     = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function produitExist(Panier $panier, Produit $produit){
        //get all ligneDePanier by panier
        $allLignes =  $this->getDoctrine()->getManager()->getRepository('AppBundle:LigneDePanier')->findBy(array('panier' => $panier));
        foreach ($allLignes as $l) {
            if ($l->getProduit() == $produit) {
                return true;
            }
        }
        //mafamech mennou fel panier
        return false;
    }

    public function calculerNbrProduitByUser(User $u, Panier $p){
        $allLignes =  $this->getDoctrine()->getManager()->getRepository('AppBundle:LigneDePanier')->findAll();
        $nbr = 0;
        foreach ($allLignes as $l) {
            if ($l->getPanier() == $p) {
                $nbr =$nbr + $l->getQuantite();
            }
        }
        return $nbr;
    }

    public function calculerPrixTotal(Panier $p){
        $allLignes =  $this->getDoctrine()->getManager()->getRepository('AppBundle:LigneDePanier')->findAll();
        $prix = 0;
        foreach ($allLignes as $l) {
            if ($l->getPanier() == $p) {
                $prix = $prix + ($l->getProduit()->getPrix() * $l->getQuantite());
            }
        }
        return $prix;
    }

    public function userHasPanier(User $u){
        $panier =  $this->getDoctrine()->getManager()->getRepository('AppBundle:Panier')->findOneBy(array('user'=> $u));
        if ($panier){
            return true;
        } else {return false;}
    }


    /**
     *
     * @Route("/rechercheProduit", name="front_produit_recherche")
     * @Method({"GET", "POST"})
     */
    public function rechercheAction(Request $request)
    {
        $em      = $this->getDoctrine()->getManager();
        $keyWord = $request->get('keyWord');

        $produits = $em->getRepository('AppBundle:Produit')->findProduct($keyWord);

        $template = $this->render(
            '@App/Produit/front/indexRecherche.html.twig',
            [
                'produits' => $produits,
            ]
        )->getContent();

        $json     = json_encode($template);
        $response = new Response($json, 200);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

}