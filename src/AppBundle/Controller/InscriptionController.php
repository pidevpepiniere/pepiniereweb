<?php
/**
 * Created by PhpStorm.
 * User: arafe
 * Date: 11/04/2019
 * Time: 13:16
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("inscription")
 */
class InscriptionController extends Controller
{
    /**
     * @Route("/", name="inscription")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request){
        $user = new User();
        $form = $this->createForm(  'AppBundle\Form\UserType', $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('homepage', array('id' => $user->getId()));
        }
        return $this->render('@App/inscription.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}