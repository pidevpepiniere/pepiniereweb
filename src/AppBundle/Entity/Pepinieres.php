<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Pepinieres
 *
 * @ORM\Table(name="pepinieres", indexes={@ORM\Index(name="user_pep", columns={"id_user"})})
 * @ORM\Entity
 */
class Pepinieres
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_pep", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPep;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_pep", type="string", length=30, nullable=false)
     */
    private $nomPep;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_pep", type="string", length=30, nullable=false)
     */
    private $adressePep;

    /**
     * @var string
     *
     * @ORM\Column(name="surface", type="string", length=30, nullable=false)
     */
    private $surface;

    /**
     * @var integer
     * @Assert\GreaterThan(-1)
     * @ORM\Column(name="max", type="integer", nullable=false)
     */
    private $max;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @return int
     */
    public function getIdPep()
    {
        return $this->idPep;
    }

    /**
     * @param int $idPep
     */
    public function setIdPep($idPep)
    {
        $this->idPep = $idPep;
    }
    /**
     * Pepinieres constructor.
     *
     * @param int $idPep
     */
    public function __construct($idPep)
    {
        $this->produits = new ArrayCollection();
    }
    /**
     * @return string
     */
    public function getNomPep()
    {
        return $this->nomPep;
    }

    /**
     * @param string $nomPep
     */
    public function setNomPep($nomPep)
    {
        $this->nomPep = $nomPep;
    }

    /**
     * @return string
     */
    public function getAdressePep()
    {
        return $this->adressePep;
    }

    /**
     * @param string $adressePep
     */
    public function setAdressePep($adressePep)
    {
        $this->adressePep = $adressePep;
    }

    /**
     * @return string
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * @param string $surface
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax($max)
    {
        $this->max = $max;
    }

    /**
     * @return User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param User $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }


    function __toString()
    {
        return $this->getNomPep()  ;
    }
}

