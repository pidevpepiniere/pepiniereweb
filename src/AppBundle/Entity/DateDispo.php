<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DateDispo
 *
 * @ORM\Table(name="date_dispo", indexes={@ORM\Index(name="dispo_cv", columns={"id_cv"})})
 * @ORM\Entity
 */
class DateDispo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_date", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="datetime", nullable=false)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="datetime", nullable=false)
     */
    private $dateFin;

    /**
     * @var \AppBundle\Entity\Cv
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cv", referencedColumnName="id_cv")
     * })
     */
    private $idCv;


}

