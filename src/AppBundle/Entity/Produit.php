<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Produit
 *
 * @Vich\Uploadable
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_prod", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProd;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=30, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=30, nullable=false)
     */
    private $nom;

    /**
     * @Vich\UploadableField(mapping="image_produit", fileNameProperty="image")
     * @var File
     */
    private $imageProduit;

    /**
     * @var string
     *
     * @ORM\Column(name="image_prod", type="text", length=65535, nullable=false)
     */
    private $image;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     * @var \DateTime
     */
    private $imageUpdatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="prix", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="disponibilite", type="string", length=20, nullable=false)
     */
    private $disponibilite;

    /**
     * @var string
     *
     * @ORM\Column(name="description_prod", type="text", length=65535, nullable=false)
     */
    private $descriptionProd;

    /**
     * @var \AppBundle\Entity\Pepinieres
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pepinieres")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pep", referencedColumnName="id_pep")
     * })
     */
    private $idPep;

    /**
     * @var integer
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;

    /**
     * @var CategorieProd
     *
     * @ORM\ManyToOne(targetEntity="CategorieProd")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id", onDelete="CASCADE")
     *
     */
    private $categorie;

    /**
     * @return int
     */
    public function getIdProd()
    {
        return $this->idProd;
    }

    /**
     * @param int $idProd
     */
    public function setIdProd($idProd)
    {
        $this->idProd = $idProd;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }


    /**
     * @return string
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param string $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return string
     */
    public function getDisponibilite()
    {
        return $this->disponibilite;
    }

    /**
     * @param string $disponibilite
     */
    public function setDisponibilite($disponibilite)
    {
        $this->disponibilite = $disponibilite;
    }

    /**
     * @return string
     */
    public function getDescriptionProd()
    {
        return $this->descriptionProd;
    }

    /**
     * @param string $descriptionProd
     */
    public function setDescriptionProd($descriptionProd)
    {
        $this->descriptionProd = $descriptionProd;
    }

    /**
     * @return Pepinieres
     */
    public function getIdPep()
    {
        return $this->idPep;
    }

    /**
     * @param Pepinieres $idPep
     */
    public function setIdPep($idPep)
    {
        $this->idPep = $idPep;
    }

    /**
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return CategorieProd
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param CategorieProd $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return File
     */
    public function getImageProduit()
    {
        return $this->imageProduit;
    }

    /**
     * @param File $imageProduit
     */
    public function setImageProduit($imageProduit)
    {
        $this->imageProduit = $imageProduit;
        if ($imageProduit instanceof UploadedFile) {
            $this->setImageUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return \DateTime
     */
    public function getImageUpdatedAt()
    {
        return $this->imageUpdatedAt;
    }

    /**
     * @param \DateTime $imageUpdatedAt
     */
    public function setImageUpdatedAt($imageUpdatedAt)
    {
        $this->imageUpdatedAt = $imageUpdatedAt;
    }


}

