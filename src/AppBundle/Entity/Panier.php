<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Panier
 *
 * @ORM\Table(name="panier")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PanierRepository")
 */
class Panier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * @var int
     *
     * @ORM\Column(name="nbr_produit", type="integer")
     */
    private $nbrProduit;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="LigneDePanier",mappedBy="panier")
     */
    private $lignedepaniers;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string",length=30, nullable=false)
     */
    private $etat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="confirmedAt", type="datetime", nullable=true)
     */
    private $confirmedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return Panier
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set nbrProduit
     *
     * @param integer $nbrProduit
     *
     * @return Panier
     */
    public function setNbrProduit($nbrProduit)
    {
        $this->nbrProduit = $nbrProduit;

        return $this;
    }

    /**
     * Get nbrProduit
     *
     * @return int
     */
    public function getNbrProduit()
    {
        return $this->nbrProduit;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignedepaniers = new ArrayCollection();
        $this->etat = "vide";
        $this->confirmedAt = new \DateTime('now');
    }

    /**
     * Add LigneDePanier
     *
     * @param LigneDePanier $lignedepanier
     *
     * @return Panier
     */
    public function addLignedepanier(LigneDePanier $lignedepanier)
    {
        $this->lignedepaniers[] = $lignedepanier;

        return $this;
    }

    /**
     * Remove LigneDePanier
     *
     * @param LigneDePanier $lignedepanier
     */
    public function removeLignedepanier(LigneDePanier $lignedepanier)
    {
        $this->lignedepaniers->removeElement($lignedepanier);
    }

    /**
     * Get lignedepaniers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignedepaniers()
    {
        return $this->lignedepaniers;
    }

    /**
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param string $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return \DateTime
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * @param \DateTime $confirmedAt
     */
    public function setConfirmedAt($confirmedAt)
    {
        $this->confirmedAt = $confirmedAt;
    }
}
