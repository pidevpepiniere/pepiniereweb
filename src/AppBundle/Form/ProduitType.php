<?php

namespace AppBundle\Form;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ProduitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type')
                ->add('nom')
                ->add('imageProduit',VichFileType::class, [
                    'required' => false,
                    'allow_delete' => true,
                    'download_link' => true
                ])
                ->add('prix',IntegerType::class, ['attr'=> ['min' =>0]])
                ->add('descriptionProd')
                ->add('stock',IntegerType::class, ['attr'=> ['min' =>0]])
                ->add('idPep', EntityType::class, [
                    'class'=>'AppBundle:Pepinieres',
                    'required'=>true,
                    'label' => 'Pepiniére',
                ],'first')
                ->add('categorie');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Produit'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_produit';
    }


}
