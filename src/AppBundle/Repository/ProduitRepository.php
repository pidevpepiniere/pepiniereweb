<?php
/**
 * Created by PhpStorm.
 * User: arafe
 * Date: 09/04/2019
 * Time: 19:04
 */

namespace AppBundle\Repository;

class ProduitRepository extends \Doctrine\ORM\EntityRepository
{
    public function findProduct($keyWord)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $q  = $qb->select(array('p'))
                 ->from('AppBundle:Produit', 'p')
                 ->where('p.nom LIKE :motClee')
                 ->orWhere('p.descriptionProd LIKE :motClee')
                 ->orWhere('p.type LIKE :motClee')
                 ->setParameter('motClee', '%'.$keyWord.'%')
                 ->getQuery();
        return $q->getResult();
    }
}